
package luka8088;

import luka8088.mocker.Entry;
import luka8088.mocker.grammar.Mixed;
import luka8088.mocker.Grammar;
import luka8088.mocker.GrammarProcessor;

public class Mocker {

  protected String seed = "";
  protected Grammar grammar = null;

  public Mocker (String seed) {
    this.seed = seed;
    this.setGrammar(new Mixed());
  }

  public void setGrammar (Grammar grammar) {
    this.grammar = grammar;
  }

  public Entry offset (String offset) {
    return new Entry(new GrammarProcessor(this.grammar), this.seed, offset);
  }

}

