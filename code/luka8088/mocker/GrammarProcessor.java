
package luka8088.mocker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import luka8088.mocker.Grammar;

public class GrammarProcessor {

  public GrammarProcessor (Grammar unprocessedGrammar) {

    ArrayList<String> beginnings = new ArrayList<String>();

    for (String beginning: unprocessedGrammar.getBeginnings())
      if (!beginnings.contains(beginning.toLowerCase().trim()))
        beginnings.add(beginning.toLowerCase().trim());

    for (String beginning: unprocessedGrammar.getSyllables())
      if (!beginnings.contains(beginning.toLowerCase().trim()))
        beginnings.add(beginning.toLowerCase().trim());

    this.beginnings = beginnings.toArray(new String[] {});

    Arrays.sort(this.beginnings);

    Map<String, String[]> map = new HashMap<String, String[]>();

    for (String mapping: unprocessedGrammar.getMap()) {

      String[] parts = mapping.split("-");

      if (!map.containsKey(parts[0].toLowerCase().trim())) {
        map.put(parts[0].toLowerCase().trim(), new String[] {});
      }

      if (Arrays.asList(map.get(parts[0].toLowerCase().trim())).contains(parts[1].toLowerCase().trim()))
        continue;

      ArrayList<String> temp1 = new ArrayList<String>(Arrays.asList(map.get(parts[0].toLowerCase().trim())));
      temp1.add(parts[1].toLowerCase().trim());
      String[] temp2 = temp1.toArray(new String[] {});
      Arrays.sort(temp2);

      map.put(parts[0].toLowerCase().trim(), temp2);

    }

    for (String mapKey: unprocessedGrammar.getSyllables()) {
      for (String mapValue: unprocessedGrammar.getSyllables()) {

        if (!map.containsKey(mapKey.toLowerCase().trim())) {
          map.put(mapKey.toLowerCase().trim(), new String[] {});
        }

        if (Arrays.asList(map.get(mapKey.toLowerCase().trim())).contains(mapValue.toLowerCase().trim()))
          continue;

        ArrayList<String> temp1 = new ArrayList<String>(Arrays.asList(map.get(mapKey.toLowerCase().trim())));
        temp1.add(mapValue.toLowerCase().trim());
        String[] temp2 = temp1.toArray(new String[] {});
        Arrays.sort(temp2);

        map.put(mapKey.toLowerCase().trim(), temp2);

      }
    }

    this.map = map;

    this.rejectedMap = unprocessedGrammar.getRejectedMap();

  }

  protected String[] beginnings;

  public String[] getBeginnings () {
    return this.beginnings;
  }

  protected Map<String, String[]> map;

  public Map<String, String[]> getMap () {
    return this.map;
  }

  protected String[] rejectedMap;

  public String[] getRejectedMap () {
    return this.rejectedMap;
  }

}
