
package luka8088.mocker;

import java.lang.StringBuilder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Function;
import javax.xml.bind.DatatypeConverter;
import luka8088.mocker.GrammarProcessor;

public class Entry {

  protected GrammarProcessor grammarProcessor = null;
  protected String seed = "";
  protected String offset = "";

  public Entry (GrammarProcessor grammarProcessor, String seed, String offset) {
    this.grammarProcessor = grammarProcessor;
    this.seed = seed;
    this.offset = offset;
  }

  public Entry offset (String offset) {
    return new Entry(this.grammarProcessor, this.seed, this.offset + "." + offset);
  }

  /**
   * Query a number among procedurally generated data.
   *
   * @param variance How many variances of looked up type should there be.
   */
  public int number (int variance) throws NoSuchAlgorithmException {
    String offset = this.seed + this.offset;
    String offsetHash = DatatypeConverter.printHexBinary(MessageDigest.getInstance("SHA-1").digest(offset.getBytes()));
    return Integer.valueOf(offsetHash.substring(0, Integer.toHexString(variance - 1).length()), 16) % variance;
  }

  /**
   * Query an element from set among procedurally generated data.
   *
   * @param set Predefined set to pick from.
   */
  public <ElementType> ElementType element (ElementType[] set) throws NoSuchAlgorithmException {
    return set[this.number(set.length)];
  }

  /**
   * Query a word among procedurally generated data.
   */
  public String word () throws NoSuchAlgorithmException {
    return this.word(3, 10);
  }

  /**
   * Query a word among procedurally generated data.
   *
   * @param minimumLetters Minimum number of letters.
   * @param maximumLetters Maximum number of letters.
   */
  public String word (int minimumLetters, int maximumLetters) throws NoSuchAlgorithmException {

    int wordLength = minimumLetters + this.number(maximumLetters - minimumLetters);

    ArrayList<String> letters = new ArrayList<String>();

    while (Entry._concate(letters.toArray(new String[] {})).length() < wordLength) {

      if (letters.size() == 0) {
        letters.add(
          this.offset("__letter__" + String.valueOf(letters.size())).element(this.grammarProcessor.getBeginnings())
        );
        continue;
      }

      if (!this.grammarProcessor.getMap().containsKey(letters.get(letters.size() - 1))) {
        throw new Error("Mapping for *" + letters.get(letters.size() - 1) + "* not found.");
      }

      ArrayList<String> candidates = new ArrayList<String>();

      for (String candidate: this.grammarProcessor.getMap().get(letters.get(letters.size() - 1))) {

        Boolean acceptCandidate = true;

        for (String rejected: this.grammarProcessor.getRejectedMap()) {

          String candidateWord = "-" + Entry._concate(letters.toArray(new String[] {}), "-") + "-" + candidate;

          if (candidateWord.endsWith("-" + rejected)) {
            acceptCandidate = false;
            break;
          }

        }

        if (acceptCandidate)
          candidates.add(candidate);

      }

      letters.add(
        this.offset("__letter__" + String.valueOf(letters.size())).element(candidates.toArray(new String[] {}))
      );

    }

    return Entry._concate(letters.toArray(new String[] {}));

  }

  /**
   * Query a text among procedurally generated data.
   */
  public String text () throws NoSuchAlgorithmException {
    return this.text(200);
  }

  /**
   * Query a text among procedurally generated data.
   *
   * @param wordCount Number of words to be generated.
   */
  public String text (int wordCount) throws NoSuchAlgorithmException {

    ArrayList<String> words = new ArrayList<String>();

    int sentenceOffset = 0;
    int wordInSentenceOffset = 0;

    for (int i = 0; i < wordCount; i += 1) {
      if (wordInSentenceOffset == 4 + this.offset("__sentence__" + String.valueOf(sentenceOffset)).number(12)) {
        sentenceOffset += 1;
        wordInSentenceOffset = 0;
        words.add(".");
      }
      String word = this.offset("__word__" + String.valueOf(i)).word();
      if (wordInSentenceOffset == 0) {
        word = word.substring(0, 1).toUpperCase() + word.substring(1);
      }
      words.add(word);
      wordInSentenceOffset += 1;
    }

    words.add(".");

    return Entry._concate(words.toArray(new String[] {}), " ").replace(" .", ".");

  }

  private static String _concate (String[] input) {
    return Entry._concate(input, "");
  }

  private static String _concate (String[] input, String delimiter) {
    StringBuilder output = new StringBuilder();
    for (int i = 0; i < input.length; i += 1) {
      output.append(input[i]);
      if (i != input.length - 1) {
        output.append(delimiter);
      }
    }
    return output.toString();
  }

}

