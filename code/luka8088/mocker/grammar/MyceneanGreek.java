
package luka8088.mocker.grammar;

import luka8088.mocker.Grammar;

/**
 * According to https://en.wikipedia.org/wiki/Linear_B#Syllabic_signs
 */
public class MyceneanGreek implements Grammar {

  public String[] getSyllables () {
    return new String[] {
      "a", "ai", "au",
      "da", "de", "di", "do", "du", "dwe", "dwo",
      "e",
      "ha",
      "i",
      "ja", "je", "jo", "ju",
      "ka", "ke", "ki", "ko", "ku",
      "ma", "me", "mi", "mo", "mu",
      "na", "ne", "ni", "no", "nu", "nwa",
      "o",
      "pa", "pe", "phu", "pi", "po", "pte", "pu",
      "qa", "qe", "qi", "qo",
      "ra", "rai", "re", "ri", "ro", "ru", "rya", "ryo",
      "sa", "se", "si", "so", "su", "swa", "swi",
      "ta", "te", "ti", "to", "tu", "twe", "two", "tya",
      "u",
      "wa", "we", "wi", "wo",
      "za", "ze", "zo", "zu",
    };
  }

  public String[] getBeginnings () {
    return new String[] {};
  }

  public String[] getMap () {
    return new String[] {};
  }

  public String[] getRejectedMap () {
    return new String[] {};
  }

}
