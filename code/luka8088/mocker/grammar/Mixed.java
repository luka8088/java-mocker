
package luka8088.mocker.grammar;

import luka8088.mocker.Grammar;

public class Mixed implements Grammar {

  public String[] getSyllables () {
    return new String[] {
      "a", "ai", "au",
      "ba", "be", "bi", "bo", "bu", "bya", "byo", "byu",
      "ca", "ce", "chi", "ci", "co", "cu",
      "da", "de", "di", "do", "du", "dwe", "dwo", "dyo", "dyu",
      "e", "en",
      "fa", "fe", "fi", "fo", "fu",
      "ga", "ge", "gi", "go", "gu", "gya", "gyo", "gyu",
      "ha", "he", "hi", "ho", "hya", "hyo", "hyu",
      "i",
      "ja", "je", "ji", "jo", "ju",
      "ka", "ke", "ki", "ko", "kom", "ku", "kwa", "kya", "kyo", "kyu",
      "la", "le", "li", "lo", "lu",
      "ma", "me", "mi", "mo", "mu", "mya", "myo", "myu",
      "n", "na", "ne", "nem", "ni", "no", "nu", "nwa", "nya", "nyo", "nyu",
      "o",
      "pa", "pe", "phu", "pi", "po", "pte", "pu", "pya", "pyo", "pyu",
      "qa", "qe", "qi", "qo", "que", "qui",
      "ra", "rai", "re", "ri", "ro", "ru", "rya", "ryo", "ryu",
      "sa", "se", "shi", "si", "so", "su", "swa", "swi", "sya", "syo", "syu",
      "ta", "te", "tem", "ten", "ti", "to", "tsu", "tu", "twe", "two", "tya", "tyo", "tyu",
      "u", "un",
      "vu",
      "wa", "wan", "we", "wi", "wo",
      "ya", "ye", "yo", "yu",
      "za", "ze", "zo", "zu", "zya", "zyo", "zyu",
    };
  }

  public String[] getBeginnings () {
    return new String[] {};
  }

  public String[] getMap () {
    return new String[] {};
  }

  public String[] getRejectedMap () {
    return new String[] {};
  }

}
