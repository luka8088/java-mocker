
package luka8088.mocker.grammar;

import luka8088.mocker.Grammar;

/**
 * According to https://en.wikipedia.org/wiki/Afaka_syllabary#Variants_and_syllabic_order
 */
public class Afaka implements Grammar {

  public String[] getSyllables () {
    return new String[] {
      "a",
      "ba", "be", "bi", "bo", "bu",
      "da", "de", "di", "do", "du", "dyo", "dyu",
      "e", "en",
      "fa", "fe", "fi", "fo", "fu",
      "ga", "ge", "gi", "go", "gu",
      "i",
      "ka", "ke", "ki", "ko", "kom", "ku", "kwa",
      "la", "le", "li", "lo", "lu",
      "ma", "me", "mi", "mo", "mu",
      "na", "ne", "nem",
      "ni", "no", "nu", "nya",
      "o",
      "pa", "pe", "pi", "po", "pu",
      "sa", "se", "si", "so", "su",
      "ta", "te", "tem", "ten", "ti", "to", "tu", "tya",
      "u", "un",
      "wa", "wan", "we", "wi",
      "ya", "ye", "yu",
    };
  }

  public String[] getBeginnings () {
    return new String[] {};
  }

  public String[] getMap () {
    return new String[] {};
  }

  public String[] getRejectedMap () {
    return new String[] {};
  }

}
