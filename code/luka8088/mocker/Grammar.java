
package luka8088.mocker;

public interface Grammar {

  public String[] getSyllables ();

  public String[] getBeginnings ();

  public String[] getMap ();

  public String[] getRejectedMap ();

}
